<?php

namespace App\Tests\Exception;

use App\Exception\ArtworkTypeNotFoundException;
use App\Tests\RandomTestCaseTrait;
use PHPUnit\Framework\TestCase;

/**
 * Class ArtworkTypeNotFoundException.
 *
 * @coversDefaultClass \App\Exception\ArtworkTypeNotFoundException
 */
class ArtworkTypeNotFoundExceptionTest extends TestCase
{
    use RandomTestCaseTrait;

    /**
     * @test
     * @covers ::__construct
     */
    public function constructTest(): void
    {
        $message = 'The needed artwork type '.$this->randomString().' is not found';
        $this->expectException(ArtworkTypeNotFoundException::class);
        $this->expectExceptionMessage($message);

        throw new ArtworkTypeNotFoundException($message);
    }
}
