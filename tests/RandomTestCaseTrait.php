<?php

namespace App\Tests;

trait RandomTestCaseTrait
{
    public function randomString(): string
    {
        return md5((string) $this->randomInteger(6));
    }

    /**
     * @param int $length
     *
     * @return int
     */
    public function randomInteger($length = 2): int
    {
        return rand(pow(10, $length - 1), pow(10, $length) - 1);
    }

    public function invalidDataForJsonDecode(): string
    {
        return urldecode('bad utf string %C4_');
    }
}
