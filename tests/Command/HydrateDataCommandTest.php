<?php

namespace App\Tests\Command;

use App\Command\HydrateDataCommand;
use App\Hydrator\Hydrator;
use App\Tests\RandomTestCaseTrait;
use App\Tests\ReflectionTestCaseTrait;
use Exception;
use Generator;
use PHPUnit\Framework\ExpectationFailedException;
use function PHPUnit\Framework\once;
use PHPUnit\Framework\TestCase;
use ReflectionException;
use SebastianBergmann\RecursionContext\InvalidArgumentException;
use Symfony\Component\Console\Exception\LogicException;
use Symfony\Component\Console\Formatter\OutputFormatterInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class HydrateDataCommand.
 *
 * @coversDefaultClass \App\Command\HydrateDataCommand
 */
class HydrateDataCommandTest extends TestCase
{
    use ReflectionTestCaseTrait;
    use RandomTestCaseTrait;

    /**
     * @var HydrateDataCommand|null
     */
    private ?HydrateDataCommand $command;

    /**
     * @var Hydrator|null
     */
    private ?Hydrator $hydrator;

    /**
     * @var InputInterface|null
     */
    private ?InputInterface $input;

    /**
     * @var OutputInterface|null
     */
    private ?OutputInterface $output;

    public function outputMock(): void
    {
        $this->output = $this->createMock(OutputInterface::class);

        $outputFormatter = $this->createMock(OutputFormatterInterface::class);

        $outputFormatter
            ->method('isDecorated')
            ->willReturn(true);

        $this->output
            ->method('getFormatter')
            ->willReturn($outputFormatter);
    }

    /**
     * @throws LogicException
     */
    public function setUp(): void
    {
        $this->hydrator = $this->createMock(Hydrator::class);

        $this->input = $this->createMock(InputInterface::class);
        $this->outputMock();

        $this->command = new HydrateDataCommand($this->hydrator);
    }

    public function tearDown(): void
    {
        $this->hydrator = null;

        $this->input = null;
        $this->output = null;

        $this->command = null;
    }

    /**
     * @test
     * @covers ::configure
     *
     * @throws ExpectationFailedException
     * @throws Exception
     * @throws InvalidArgumentException
     */
    public function configureTest(): void
    {
        self::assertSame(
            'Hydrate the PostgreSQL database with the Star Wars artworks.',
            $this->command->getDescription()
        );

        self::assertSame(
            'This command create all artworks in database, but it not removes them.',
            $this->command->getHelp()
        );

        self::assertSame(1, $this->command->getDefinition()->getArgumentCount());
        $argumentLists = $this->command->getDefinition()->getArguments();
        $firstArg = array_shift($argumentLists);
        self::assertSame('type', $firstArg->getName());
        self::assertSame('the artwork type', $firstArg->getDescription());
        self::assertNull($firstArg->getDefault());
    }

    /**
     * @test
     * @covers ::__construct
     *
     * @throws ExpectationFailedException
     * @throws Exception
     * @throws InvalidArgumentException
     */
    public function contructTest(): void
    {
        self::assertInstanceOf(HydrateDataCommand::class, $this->command);
    }

    /**
     * @test
     * @covers ::execute
     * @dataProvider dataExecuteWithParam
     *
     * @param string $param
     *
     * @throws ReflectionException
     */
    public function executeOnlyOneTest(string $param): void
    {
        $this->output
            ->expects(self::exactly(2))
            ->method('writeln')
            ->withConsecutive(['Starting to hydrate database...'], [PHP_EOL.'Finish!']);

        $this->input->expects(self::once())
            ->method('getArgument')
            ->with('type')
            ->willReturn($param);

        $this->hydrator->expects(self::once())->method('hydrate');

        $return = $this->callPrivateMethod($this->command, 'execute', [$this->input, $this->output]);

        self::assertSame(0, $return);
    }

    /**
     * @test
     * @covers ::execute
     * @dataProvider dataExecuteWithParam
     *
     * @throws ReflectionException
     */
    public function executeAllTest(): void
    {
        $this->output
            ->expects(self::exactly(2))
            ->method('writeln')
            ->withConsecutive(['Starting to hydrate database...'], [PHP_EOL.'Finish!']);

        $this->input
            ->expects(self::once())
            ->method('getArgument')
            ->with('type')
            ->willReturn(null);

        $this->hydrator
            ->expects(self::exactly(3))
            ->method('hydrate');

        $return = $this->callPrivateMethod($this->command, 'execute', [$this->input, $this->output]);

        self::assertSame(0, $return);
    }

    /**
     * @return Generator
     */
    public function dataExecuteWithParam(): Generator
    {
        yield 'ANIMATED_MOVIE' => ['ANIMATED_MOVIE'];
        yield 'MOVIE' => ['MOVIE'];
        yield 'VIDEO_GAME' => ['VIDEO_GAME'];
    }

    /**
     * @test
     * @covers ::execute
     *
     * @throws ReflectionException
     */
    public function executeIntergerParamTest(): void
    {
        $this->output
            ->expects(self::exactly(2))
            ->method('writeln')
            ->withConsecutive(
                ['Starting to hydrate database...'],
                [PHP_EOL.'The artwork type parameter must be a string']
            );

        $this->input->expects(self::once())
            ->method('getArgument')
            ->with('type')
            ->willReturn($this->randomInteger());

        $this->hydrator->expects(self::never())->method('hydrate');

        $return = $this->callPrivateMethod($this->command, 'execute', [$this->input, $this->output]);

        self::assertSame(1, $return);
    }

    /**
     * @test
     * @covers ::execute
     *
     * @throws ReflectionException
     */
    public function executeArrayParamTest(): void
    {
        $this->output
            ->expects(self::exactly(2))
            ->method('writeln')
            ->withConsecutive(
                ['Starting to hydrate database...'],
                [PHP_EOL.'The artwork type parameter must be a string']
            );

        $this->input->expects(self::once())
            ->method('getArgument')
            ->with('type')
            ->willReturn([$this->randomString(), $this->randomString()]);

        $this->hydrator->expects(self::never())->method('hydrate');

        $return = $this->callPrivateMethod($this->command, 'execute', [$this->input, $this->output]);

        self::assertSame(1, $return);
    }

    /**
     * @test
     * @covers ::execute
     *
     * @throws ReflectionException
     */
    public function executeWrongArtworkTypeTest(): void
    {
        $type = $this->randomString();
        $this->output
            ->expects(self::exactly(2))
            ->method('writeln')
            ->withConsecutive(
                ['Starting to hydrate database...'],
                [PHP_EOL.'The needed artwork type '.$type.' is not found']
            );

        $this->input->expects(self::once())
            ->method('getArgument')
            ->with('type')
            ->willReturn($type);

        $this->hydrator->expects(self::never())->method('hydrate');

        $return = $this->callPrivateMethod($this->command, 'execute', [$this->input, $this->output]);

        self::assertSame(1, $return);
    }

    /**
     * @test
     * @covers ::execute
     *
     * @throws ReflectionException
     */
    public function executeExceptionTest(): void
    {
        $errorMessage = $this->randomString();
        $this->output
            ->expects(self::exactly(2))
            ->method('writeln')
            ->withConsecutive(
                ['Starting to hydrate database...'],
                [PHP_EOL.$errorMessage]
            );

        $this->input->expects(self::once())
            ->method('getArgument')
            ->with('type')
            ->willReturn(null);

        $this->hydrator
            ->expects(self::once())
            ->method('hydrate')
            ->willThrowException(new Exception($errorMessage));

        $return = $this->callPrivateMethod($this->command, 'execute', [$this->input, $this->output]);

        self::assertSame(1, $return);
    }

    /**
     * @test
     * @covers ::hydrateOne
     *
     * @throws ReflectionException
     */
    public function hydrateOneTest(): void
    {
        $type = $this->randomString();
        $this->hydrator->expects(once())->method('hydrate')->with($type);

        $this->callPrivateMethod($this->command, 'hydrateOne', [$type]);
    }

    /**
     * @test
     * @covers ::hydrateAll
     *
     * @throws ReflectionException
     */
    public function hydrateAllTest(): void
    {
        $this->hydrator
            ->expects(self::exactly(3))
            ->method('hydrate')
            ->withConsecutive(['ANIMATED_MOVIE'], ['MOVIE'], ['VIDEO_GAME']);

        $this->callPrivateMethod($this->command, 'hydrateAll');
    }
}
