<?php

namespace App\Tests\Command;

use App\Command\HydrateRedisCommand;
use App\Entity\Artwork;
use App\Repository\ArtworkRepository;
use App\Tests\RandomTestCaseTrait;
use App\Tests\ReflectionTestCaseTrait;
use Exception;
use PHPUnit\Framework\ExpectationFailedException;
use PHPUnit\Framework\TestCase;
use Predis\ClientInterface;
use ReflectionException;
use SebastianBergmann\RecursionContext\InvalidArgumentException;
use Symfony\Component\Console\Exception\LogicException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class HydrateRedisCommand.
 *
 * @coversDefaultClass \App\Command\HydrateRedisCommand
 */
class HydrateRedisCommandTest extends TestCase
{
    use ReflectionTestCaseTrait;
    use RandomTestCaseTrait;

    /**
     * @var HydrateRedisCommand|null
     */
    private ?HydrateRedisCommand $command;

    /**
     * @var ClientInterface|null
     */
    private ?ClientInterface $redisClient;

    /**
     * @var ArtworkRepository|null
     */
    protected ?ArtworkRepository $artworkRepository;

    /**
     * @var InputInterface|null
     */
    private ?InputInterface $input;

    /**
     * @var OutputInterface|null
     */
    private ?OutputInterface $output;

    /**
     * @throws LogicException
     */
    public function setUp(): void
    {
        $this->redisClient = $this->createMock(ClientInterface::class);
        $this->artworkRepository = $this->createMock(ArtworkRepository::class);

        $this->input = $this->createMock(InputInterface::class);
        $this->output = $this->createMock(OutputInterface::class);

        $this->command = new HydrateRedisCommand($this->redisClient, $this->artworkRepository);
    }

    public function tearDown(): void
    {
        $this->redisClient = null;
        $this->artworkRepository = null;

        $this->input = null;
        $this->output = null;

        $this->command = null;
    }

    /**
     * @test
     * @covers ::configure
     *
     * @throws ExpectationFailedException
     * @throws Exception
     * @throws InvalidArgumentException
     */
    public function configureTest(): void
    {
        self::assertSame(
            'Hydrate the Redis DB with the PostgreSQL data.',
            $this->command->getDescription()
        );

        self::assertSame(
            'This command will get all artworks from PostgreSQL, format and push them into the Redis DB.',
            $this->command->getHelp()
        );

        self::assertSame(0, $this->command->getDefinition()->getArgumentCount());
    }

    /**
     * @test
     * @covers ::__construct
     *
     * @throws ExpectationFailedException
     * @throws Exception
     * @throws InvalidArgumentException
     */
    public function contructTest(): void
    {
        self::assertInstanceOf(HydrateRedisCommand::class, $this->command);
    }

    /**
     * @test
     * @covers ::execute
     *
     * @throws ReflectionException
     */
    public function executeTest(): void
    {
        $this->output
            ->expects(self::exactly(2))
            ->method('writeln')
            ->withConsecutive(
                ['Starting to hydrate Redis DB...'],
                [PHP_EOL.'Finish!']
            );

        $artwork1 = $this->createMock(Artwork::class);
        $artwork1Id = $this->randomInteger();
        $artwork1RedisFormat = $this->randomString();

        $artwork2 = $this->createMock(Artwork::class);
        $artwork2Id = $this->randomInteger();
        $artwork2RedisFormat = $this->randomString();

        $artworks = [$artwork1, $artwork2];
        $this->artworkRepository->expects(self::once())
            ->method('findBy')
            ->with([], ['abyYearStart' => 'ASC', 'abyYearEnd' => 'ASC'])
            ->willReturn($artworks);

        $artwork1->expects(self::exactly(2))
            ->method('getId')
            ->willReturn($artwork1Id);

        $artwork1->expects(self::once())
            ->method('redisFormat')
            ->willReturn($artwork1RedisFormat);

        $artwork2->expects(self::exactly(2))
            ->method('getId')
            ->willReturn($artwork2Id);

        $artwork2->expects(self::once())
            ->method('redisFormat')
            ->willReturn($artwork2RedisFormat);

        $return = $this->callPrivateMethod($this->command, 'execute', [$this->input, $this->output]);

        self::assertSame(0, $return);
    }

    /**
     * @test
     * @covers ::execute
     *
     * @throws ReflectionException
     */
    public function executeThrowExceptionTest(): void
    {
        $errorMessage = $this->randomString();
        $this->output
            ->expects(self::exactly(2))
            ->method('writeln')
            ->withConsecutive(
                ['Starting to hydrate Redis DB...'],
                [PHP_EOL.$errorMessage]
            );

        $this->artworkRepository->expects(self::once())
            ->method('findBy')
            ->willThrowException(new Exception($errorMessage));

        $return = $this->callPrivateMethod($this->command, 'execute', [$this->input, $this->output]);

        self::assertSame(1, $return);
    }
}
