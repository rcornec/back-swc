<?php

namespace App\Tests;

use ReflectionClass;
use ReflectionException;

trait ReflectionTestCaseTrait
{
    /**
     * @param object $object
     * @param string $method
     * @param array  $args
     *
     * @return mixed
     *
     * @throws ReflectionException
     */
    protected function callPrivateMethod(object $object, string $method, array $args = [])
    {
        $class = new ReflectionClass($object);
        $method = $class->getMethod($method);
        $method->setAccessible(true);

        return $method->invokeArgs($object, $args);
    }

    /**
     * Return vale of a private/protected property using ReflectionClass.
     *
     * @param        $instance
     * @param string $property
     *
     * @return mixed
     *
     * @throws ReflectionException
     */
    public function getInnerProperty($instance, string $property)
    {
        $reflector = new ReflectionClass($instance);
        $reflectorProperty = $reflector->getProperty($property);
        $reflectorProperty->setAccessible(true);

        return $reflectorProperty->getValue($instance);
    }

    /**
     * Set a value of a private/protected property using ReflectionClass.
     *
     * @param        $instance
     * @param string $property
     * @param mixed  $newValue
     *
     * @throws ReflectionException
     */
    public function setInnerProperty($instance, string $property, $newValue): void
    {
        $reflector = new ReflectionClass($instance);
        $reflectorProperty = $reflector->getProperty($property);
        $reflectorProperty->setAccessible(true);
        $reflectorProperty->setValue($instance, $newValue);
    }

    /**
     * Return value of private or protected constante.
     *
     * @param        $instance
     * @param string $constant
     *
     * @return mixed
     *
     * @throws ReflectionException
     */
    public function getInnerConstant($instance, string $constant)
    {
        $reflector = new ReflectionClass($instance);

        return $reflector->getConstant($constant);
    }
}
