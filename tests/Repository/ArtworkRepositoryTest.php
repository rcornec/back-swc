<?php

namespace App\Tests\Repository;

use App\Entity\Artwork;
use App\Repository\ArtworkRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\Persistence\ManagerRegistry;
use PHPUnit\Framework\TestCase;

/**
 * Class ArtworkRepositoryTest.
 *
 * @coversDefaultClass \App\Repository\ArtworkRepository
 */
class ArtworkRepositoryTest extends TestCase
{
    /**
     * @test
     * @covers ::__construct
     */
    public function contructTest(): void
    {
        $managerRegistry = $this->createMock(ManagerRegistry::class);
        $entityManager = $this->createMock(EntityManagerInterface::class);

        $managerRegistry->expects(self::once())
            ->method('getManagerForClass')
            ->with(Artwork::class)
            ->willReturn($entityManager);

        $entityManager->expects(self::once())
            ->method('getClassMetadata')
            ->with(Artwork::class)
            ->willReturn($this->createMock(ClassMetadata::class));

        $repository = new ArtworkRepository($managerRegistry);

        self::assertInstanceOf(ArtworkRepository::class, $repository);
    }
}
