<?php

namespace App\Tests\Repository;

use App\Entity\ArtworkType;
use App\Repository\ArtworkTypeRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\Persistence\ManagerRegistry;
use PHPUnit\Framework\TestCase;

/**
 * Class ArtworkTypeRepositoryTest.
 *
 * @coversDefaultClass \App\Repository\ArtworkTypeRepository
 */
class ArtworkTypeRepositoryTest extends TestCase
{
    /**
     * @test
     * @covers ::__construct
     */
    public function contructTest(): void
    {
        $managerRegistry = $this->createMock(ManagerRegistry::class);
        $entityManager = $this->createMock(EntityManagerInterface::class);

        $managerRegistry->expects(self::once())
            ->method('getManagerForClass')
            ->with(ArtworkType::class)
            ->willReturn($entityManager);

        $entityManager->expects(self::once())
            ->method('getClassMetadata')
            ->with(ArtworkType::class)
            ->willReturn($this->createMock(ClassMetadata::class));

        $repository = new ArtworkTypeRepository($managerRegistry);

        self::assertInstanceOf(ArtworkTypeRepository::class, $repository);
    }
}
