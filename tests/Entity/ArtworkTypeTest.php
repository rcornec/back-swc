<?php

namespace App\Tests\Entity;

use App\Entity\Artwork;
use App\Entity\ArtworkType;
use App\Tests\RandomTestCaseTrait;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use function PHPUnit\Framework\assertSame;
use PHPUnit\Framework\TestCase;

/**
 * Class ArtworkTypeTest.
 *
 * @coversDefaultClass \App\Entity\ArtworkType
 */
class ArtworkTypeTest extends TestCase
{
    use RandomTestCaseTrait;

    /**
     * @test
     * @covers ::__construct
     * @covers ::getName
     * @covers ::setName
     * @covers ::getArtworks
     * @covers ::setArtworks
     * @covers ::addArtwork
     */
    public function entityTest(): void
    {
        $name = $this->randomString();
        $artworkType = new ArtworkType($name);
        self::assertSame($name, $artworkType->getName());

        $artwork1 = new Artwork(
            $artworkType,
            $this->randomString(),
            $this->randomString(),
            $this->randomString(),
            $this->randomString(),
            new DateTime(),
            $this->randomInteger(),
            $this->randomInteger()
        );

        $artwork2 = new Artwork(
            $artworkType,
            $this->randomString(),
            $this->randomString(),
            $this->randomString(),
            $this->randomString(),
            new DateTime(),
            $this->randomInteger(),
            $this->randomInteger()
        );

        $artworksCollection = new ArrayCollection([$artwork1, $artwork2]);
        $artworkType->setArtworks($artworksCollection);

        $name2 = $this->randomString();
        $artworkType->setName($name2);

        self::assertSame($artworksCollection, $artworkType->getArtworks());
        self::assertSame($name2, $artworkType->getName());

        $emptyCollection = new ArrayCollection();
        $artworkType->setArtworks($emptyCollection);
        self::assertSame($emptyCollection, $artworkType->getArtworks());

        $artworkType->addArtwork($artwork1);
        self::assertEquals(new ArrayCollection([$artwork1]), $artworkType->getArtworks());
    }

    /**
     * @test
     * @covers ::__toString
     */
    public function toStringTest(): void
    {
        $name = $this->randomString();
        $artworkType = new ArtworkType($name);

        assertSame($name, $artworkType->__toString());
        assertSame($name, (string) $artworkType);
    }
}
