<?php

namespace App\Tests\Entity;

use App\Entity\Artwork;
use App\Entity\ArtworkType;
use App\Tests\RandomTestCaseTrait;
use App\Tests\ReflectionTestCaseTrait;
use DateTime;
use JsonException;
use PHPUnit\Framework\TestCase;
use ReflectionException;

/**
 * Class ArtworkTest.
 *
 * @coversDefaultClass \App\Entity\Artwork
 */
class ArtworkTest extends TestCase
{
    use ReflectionTestCaseTrait;
    use RandomTestCaseTrait;

    /**
     * @test
     * @covers ::redisFormat
     *
     * @throws JsonException
     */
    public function redisFormatTest(): void
    {
        $artworkType = new ArtworkType($this->randomString());
        $titleEn = $this->randomString();
        $titleFr = $this->randomString();
        $summaryEn = $this->randomString();
        $summaryFr = $this->randomString();
        $releaseDate = new DateTime();
        $abyStart = (float) $this->randomInteger();
        $abyEnd = (float) $this->randomInteger();

        $artwork = new Artwork(
            $artworkType,
            $titleEn,
            $titleFr,
            $summaryEn,
            $summaryFr,
            $releaseDate,
            $abyStart,
            $abyEnd
        );

        self::assertSame(
            json_encode([
                'title_en' => $artwork->getTitleEn(),
                'title_fr' => $artwork->getTitleFr(),
                'type' => $artwork->getType(),
                'aby_start' => $artwork->getAbyYearStart(),
                'aby_end' => $artwork->getAbyYearEnd(),
            ], JSON_THROW_ON_ERROR),
            $artwork->redisFormat()
        );
    }

    /**
     * @test
     *
     * @throws ReflectionException
     */
    public function entityTest(): void
    {
        $artworkType = new ArtworkType($this->randomString());
        $titleEn = $this->randomString();
        $titleFr = $this->randomString();
        $summaryEn = $this->randomString();
        $summaryFr = $this->randomString();
        $releaseDate = new DateTime();
        $abyStart = (float) $this->randomInteger();
        $abyEnd = (float) $this->randomInteger();

        $artwork = new Artwork(
            $artworkType,
            $titleEn,
            $titleFr,
            $summaryEn,
            $summaryFr,
            $releaseDate,
            $abyStart,
            $abyEnd
        );

        self::assertSame($artworkType, $artwork->getType());
        self::assertSame($titleEn, $artwork->getTitleEn());
        self::assertSame($titleFr, $artwork->getTitleFr());
        self::assertSame($summaryEn, $artwork->getSummaryEn());
        self::assertSame($summaryFr, $artwork->getSummaryFr());
        self::assertSame($releaseDate, $artwork->getReleaseDate());
        self::assertSame($abyStart, $artwork->getAbyYearStart());
        self::assertSame($abyEnd, $artwork->getAbyYearEnd());

        $id = $this->randomInteger();
        $artworkType2 = new ArtworkType($this->randomString());
        $titleEn2 = $this->randomString();
        $titleFr2 = $this->randomString();
        $summaryEn2 = $this->randomString();
        $summaryFr2 = $this->randomString();
        $releaseDate2 = new DateTime();
        $abyStart2 = (float) $this->randomInteger();
        $abyEnd2 = (float) $this->randomInteger();

        $this->setInnerProperty($artwork, 'id', $id);
        $artwork->setType($artworkType2);
        $artwork->setTitleEn($titleEn2);
        $artwork->setTitleFr($titleFr2);
        $artwork->setSummaryEn($summaryEn2);
        $artwork->setSummaryFr($summaryFr2);
        $artwork->setReleaseDate($releaseDate2);
        $artwork->setAbyYearStart($abyStart2);
        $artwork->setAbyYearEnd($abyEnd2);

        self::assertSame($id, $artwork->getId());
        self::assertSame($artworkType2, $artwork->getType());
        self::assertSame($titleEn2, $artwork->getTitleEn());
        self::assertSame($titleFr2, $artwork->getTitleFr());
        self::assertSame($summaryEn2, $artwork->getSummaryEn());
        self::assertSame($summaryFr2, $artwork->getSummaryFr());
        self::assertSame($releaseDate2, $artwork->getReleaseDate());
        self::assertSame($abyStart2, $artwork->getAbyYearStart());
        self::assertSame($abyEnd2, $artwork->getAbyYearEnd());
    }
}
