<?php

namespace App\Tests\Hydrator;

use App\Entity\ArtworkType;
use App\Exception\ArtworkTypeNotFoundException;
use App\Hydrator\Hydrator;
use App\Repository\ArtworkTypeRepository;
use App\Tests\RandomTestCaseTrait;
use App\Tests\ReflectionTestCaseTrait;
use Doctrine\ORM\EntityManager;
use Exception;
use Generator;
use PHPUnit\Framework\TestCase;
use ReflectionException;

/**
 * Class HydratorTest.
 *
 * @coversDefaultClass \App\Hydrator\Hydrator
 */
class HydratorTest extends TestCase
{
    use RandomTestCaseTrait;
    use ReflectionTestCaseTrait;

    /**
     * @var Hydrator|null
     */
    private ?Hydrator $hydrator;

    /**
     * @var EntityManager|null
     */
    private ?EntityManager $entityManager;

    /**
     * @var ArtworkTypeRepository|null
     */
    private ?ArtworkTypeRepository $artworkTypeRepo;

    public function setUp(): void
    {
        $this->entityManager = $this->createMock(EntityManager::class);
        $this->artworkTypeRepo = $this->createMock(ArtworkTypeRepository::class);

        $this->hydrator = new Hydrator($this->entityManager, $this->artworkTypeRepo);
    }

    public function tearDown(): void
    {
        $this->entityManager = null;
        $this->artworkTypeRepo = null;

        $this->hydrator = null;
    }

    /**
     * @test
     * @covers ::__construct
     */
    public function contructTest(): void
    {
        self::assertInstanceOf(Hydrator::class, $this->hydrator);
    }

    /**
     * @test
     * @covers ::setArtworkType
     *
     * @throws ReflectionException
     */
    public function setArtworkTypeTest(): void
    {
        $artworkTypeName = $this->randomString();
        $artworkType = new ArtworkType($artworkTypeName);

        $this->artworkTypeRepo
            ->expects(self::once())
            ->method('find')
            ->with($artworkTypeName)
            ->willReturn($artworkType);

        $this->callPrivateMethod($this->hydrator, 'setArtworkType', [$artworkTypeName]);
        self::assertSame($artworkType, $this->getInnerProperty($this->hydrator, 'artworkType'));
    }

    /**
     * @test
     * @covers ::setArtworkType
     *
     * @throws ReflectionException
     */
    public function setArtworkTypeThrowExceptionTest(): void
    {
        $artworkTypeName = $this->randomString();

        $this->artworkTypeRepo
            ->expects(self::once())
            ->method('find')
            ->with($artworkTypeName)
            ->willReturn(null);

        $this->expectException(ArtworkTypeNotFoundException::class);
        $this->expectExceptionMessage("The needed artwork type $artworkTypeName is not found");

        $this->callPrivateMethod($this->hydrator, 'setArtworkType', [$artworkTypeName]);
        self::assertNull($this->getInnerProperty($this->hydrator, 'artworkType'));
    }

    /**
     * @test
     * @covers ::hydrate
     * @dataProvider dataHydrate
     *
     * @param string $artworkTypeName
     *
     * @throws Exception
     */
    public function hydrateTest(string $artworkTypeName): void
    {
        $artworkType = new ArtworkType($this->randomString());
        $artworkType->setName($artworkTypeName);

        $this->artworkTypeRepo
            ->expects(self::once())
            ->method('find')
            ->with($artworkTypeName)
            ->willReturn($artworkType);

        $this->entityManager
            ->expects(self::atLeast(1))
            ->method('persist');

        $this->entityManager
            ->expects(self::once())
            ->method('flush');

        $this->hydrator->hydrate($artworkTypeName);
    }

    /**
     * @return Generator
     */
    public function dataHydrate(): Generator
    {
        yield ['ANIMATED_MOVIE'];
//        yield ['ANIMATED_SERIE'];
        yield ['MOVIE'];
//        yield ['SERIE'];
//        yield ['BOOK'];
//        yield ['COMIC'];
        yield ['VIDEO_GAME'];
    }

    /**
     * @test
     * @covers ::hydrate
     *
     * @throws Exception
     */
    public function hydrateThrowExceptionTest(): void
    {
        $artworkTypeName = $this->randomString();
        $artworkType = new ArtworkType($artworkTypeName);

        $this->artworkTypeRepo
            ->expects(self::once())
            ->method('find')
            ->with($artworkTypeName)
            ->willReturn($artworkType);

        $this->entityManager
            ->expects(self::never())
            ->method('persist');

        $this->entityManager
            ->expects(self::never())
            ->method('flush');

        $this->expectWarning();
        $this->expectWarningMessage(
            'file_get_contents(data/'.$artworkTypeName.'.json): failed to open stream: No such file or directory'
        );

        $this->hydrator->hydrate($artworkTypeName);
    }
}
