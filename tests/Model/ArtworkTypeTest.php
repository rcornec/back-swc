<?php

namespace App\Tests\Model;

use App\Model\ArtworkType;
use App\Tests\RandomTestCaseTrait;
use Generator;
use PHPUnit\Framework\TestCase;

/**
 * Class ArtworkTypeTest.
 *
 * @coversDefaultClass \App\Model\ArtworkType
 */
class ArtworkTypeTest extends TestCase
{
    use RandomTestCaseTrait;

    /**
     * @test
     * @covers ::exists
     * @dataProvider dataExecuteWithParam
     *
     * @param string $value
     * @param bool   $result
     */
    public function existsTest(string $value, bool $result): void
    {
        self::assertSame($result, ArtworkType::exists($value));
    }

    /**
     * @return Generator
     */
    public function dataExecuteWithParam(): Generator
    {
        yield 'ANIMATED_MOVIE' => ['ANIMATED_MOVIE', true];
        yield 'ANIMATED_SERIE' => ['ANIMATED_SERIE', true];
        yield 'BOOK' => ['BOOK', true];
        yield 'COMIC' => ['COMIC', true];
        yield 'MOVIE' => ['MOVIE', true];
        yield 'SERIE' => ['SERIE', true];
        yield 'VIDEO_GAME' => ['VIDEO_GAME', true];
        yield 'Random' => [$this->randomString(), false];
    }
}
