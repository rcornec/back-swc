<?php

namespace App\Exception;

use Symfony\Component\Config\Definition\Exception\Exception;

class WrongTypeException extends Exception
{
}
