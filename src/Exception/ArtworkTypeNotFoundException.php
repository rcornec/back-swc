<?php

namespace App\Exception;

use Symfony\Component\Config\Definition\Exception\Exception;

class ArtworkTypeNotFoundException extends Exception
{
    /**
     * ArtworkTypeNotFoundException constructor.
     *
     * @param string $artworkType
     */
    public function __construct(string $artworkType)
    {
        parent::__construct(sprintf('The needed artwork type %s is not found', $artworkType));
    }
}
