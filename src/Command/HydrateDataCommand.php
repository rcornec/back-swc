<?php

namespace App\Command;

use App\Exception\ArtworkTypeNotFoundException;
use App\Exception\WrongTypeException;
use App\Hydrator\Hydrator;
use App\Model\ArtworkType;
use Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class HydrateDataCommand.
 */
class HydrateDataCommand extends Command
{
    protected static $defaultName = 'app:hydrate-data';

    private const ARGUMENT_TYPE = 'type';

    private Hydrator $hydrator;

    /**
     * HydrateDataCommand constructor.
     *
     * @param Hydrator $hydrator
     */
    public function __construct(Hydrator $hydrator)
    {
        $this->hydrator = $hydrator;

        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->setDescription('Hydrate the PostgreSQL database with the Star Wars artworks.')
            ->setHelp('This command create all artworks in database, but it not removes them.')
            ->addArgument(self::ARGUMENT_TYPE, InputArgument::OPTIONAL, 'the artwork type')
        ;
    }

    /**
     * {@inheritDoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            $output->writeln('Starting to hydrate database...');

            $type = $input->getArgument(self::ARGUMENT_TYPE);

            if (null !== $type && !is_string($type)) {
                throw new WrongTypeException('The artwork type parameter must be a string');
            }

            if (is_string($type) && !ArtworkType::exists($type)) {
                throw new ArtworkTypeNotFoundException($type);
            }

            null === $type ? $this->hydrateAll() : $this->hydrateOne($type);

            $output->writeln(PHP_EOL.'Finish!');

            return Command::SUCCESS;
        } catch (Exception $exception) {
            $output->writeln(PHP_EOL.$exception->getMessage());

            return Command::FAILURE;
        }
    }

    /**
     * @param string $type
     *
     * @throws Exception
     */
    private function hydrateOne(string $type): void
    {
        $this->hydrator->hydrate($type);
    }

    /**
     * @throws Exception
     */
    private function hydrateAll(): void
    {
        $this->hydrator->hydrate(ArtworkType::ANIMATED_MOVIE);
        $this->hydrator->hydrate(ArtworkType::MOVIE);
        $this->hydrator->hydrate(ArtworkType::VIDEO_GAME);
    }
}
