<?php

namespace App\Command;

use App\Repository\ArtworkRepository;
use Exception;
use Predis\ClientInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class HydrateRedisCommand.
 */
class HydrateRedisCommand extends Command
{
    protected static $defaultName = 'app:hydrate-redis';

    /**
     * @var ClientInterface
     */
    private ClientInterface $redisClient;

    /**
     * @var ArtworkRepository
     */
    protected ArtworkRepository $artworkRepository;

    /**
     * HydrateRedisCommand constructor.
     *
     * @param ClientInterface   $redisClient
     * @param ArtworkRepository $artworkRepository
     */
    public function __construct(
        ClientInterface $redisClient,
        ArtworkRepository $artworkRepository
    ) {
        $this->redisClient = $redisClient;
        $this->artworkRepository = $artworkRepository;

        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->setDescription('Hydrate the Redis DB with the PostgreSQL data.')
            ->setHelp('This command will get all artworks from PostgreSQL, format and push them into the Redis DB.')
        ;
    }

    /**
     * {@inheritDoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            $output->writeln('Starting to hydrate Redis DB...');

            $artworks = $this->artworkRepository->findBy([], ['abyYearStart' => 'ASC', 'abyYearEnd' => 'ASC']);

            foreach ($artworks as $artwork) {
                $this->redisClient->del($artwork->getId());
                $this->redisClient->set($artwork->getId(), $artwork->redisFormat());
            }

            $output->writeln(PHP_EOL.'Finish!');

            return Command::SUCCESS;
        } catch (Exception $exception) {
            $output->writeln(PHP_EOL.$exception->getMessage());

            return Command::FAILURE;
        }
    }
}
