<?php

namespace App\Hydrator;

interface InterfaceHydrator
{
    public function hydrate(string $type): void;
}
