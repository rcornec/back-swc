<?php

namespace App\Hydrator;

use App\Entity\Artwork;
use App\Entity\ArtworkType as ArtworkTypeEntity;
use App\Exception\ArtworkTypeNotFoundException;
use App\Repository\ArtworkTypeRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;

/**
 * Class Hydrator.
 */
class Hydrator implements InterfaceHydrator
{
    /**
     * @var ArtworkTypeEntity
     */
    protected ArtworkTypeEntity $artworkType;

    /**
     * @var EntityManagerInterface
     */
    protected EntityManagerInterface $entityManager;

    /**
     * @var ArtworkTypeRepository
     */
    private ArtworkTypeRepository $artworkTypeRepo;

    /**
     * HydrateDataService constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param ArtworkTypeRepository  $artworkTypeRepo
     */
    public function __construct(EntityManagerInterface $entityManager, ArtworkTypeRepository $artworkTypeRepo)
    {
        $this->entityManager = $entityManager;
        $this->artworkTypeRepo = $artworkTypeRepo;
    }

    /**
     * HydrateDataService constructor.
     *
     * @param string $artworkTypeName
     */
    private function setArtworkType(string $artworkTypeName): void
    {
        $artworkType = $this->artworkTypeRepo->find($artworkTypeName);

        if (null === $artworkType) {
            throw new ArtworkTypeNotFoundException($artworkTypeName);
        }

        $this->artworkType = $artworkType;
    }

    /**
     * @param string $type
     *
     * @throws Exception
     */
    public function hydrate(string $type): void
    {
        $this->setArtworkType($type);
        $filePath = 'data/'.$this->artworkType.'.json';

        $jsonData = file_get_contents($filePath);
        $jsonArray = json_decode($jsonData, true);

        foreach ($jsonArray as $params) {
            $artwork = new Artwork(
                $this->artworkType,
                $params['title_en'],
                $params['title_fr'],
                $params['summary_en'],
                $params['summary_fr'],
                new DateTime($params['release_date']),
                $params['aby_year_start'],
                $params['aby_year_end'],
            );

            $this->entityManager->persist($artwork);
        }

        $this->entityManager->flush();
    }
}
