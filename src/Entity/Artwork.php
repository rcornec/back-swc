<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ArtworkRepository;
use DateTime;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use JsonException;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=ArtworkRepository::class)
 * @ApiResource()
 */
class Artwork
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private string $titleEn;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private string $titleFr;

    /**
     * @ORM\Column(type="text", nullable=false)
     */
    private string $summaryEn;

    /**
     * @ORM\Column(type="text", nullable=false)
     */
    private string $summaryFr;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private DateTimeInterface $releaseDate;

    /**
     * @ORM\Column(type="float", nullable=false)
     */
    private float $abyYearStart;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Assert\GreaterThan(propertyPath="abyYearStart")
     */
    private ?float $abyYearEnd;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ArtworkType", inversedBy="artworks")
     * @ORM\JoinColumn(name="type", referencedColumnName="name", nullable=false)
     */
    private ArtworkType $type;

    /**
     * @param ArtworkType $type
     * @param string      $titleEn
     * @param string      $titleFr
     * @param string      $summaryEn
     * @param string      $summaryFr
     * @param DateTime    $releaseDate
     * @param float       $abyYearStart
     * @param float|null  $abyYearEnd
     */
    public function __construct(
        ArtworkType $type,
        string $titleEn,
        string $titleFr,
        string $summaryEn,
        string $summaryFr,
        DateTime $releaseDate,
        float $abyYearStart,
        ?float $abyYearEnd = null
    ) {
        $this->setType($type);
        $this->setTitleEn($titleEn);
        $this->setTitleFr($titleFr);
        $this->setSummaryEn($summaryEn);
        $this->setSummaryFr($summaryFr);
        $this->setReleaseDate($releaseDate);
        $this->setAbyYearStart($abyYearStart);
        $this->setAbyYearEnd($abyYearEnd);
    }

    /**
     * @return string
     *
     * @throws JsonException
     */
    public function redisFormat(): string
    {
        return json_encode([
            'title_en' => $this->getTitleEn(),
            'title_fr' => $this->getTitleFr(),
            'type' => $this->getType(),
            'aby_start' => $this->getAbyYearStart(),
            'aby_end' => $this->getAbyYearEnd(),
        ], JSON_THROW_ON_ERROR);
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitleEn(): string
    {
        return $this->titleEn;
    }

    /**
     * @param string $titleEn
     */
    public function setTitleEn(string $titleEn): void
    {
        $this->titleEn = $titleEn;
    }

    /**
     * @return string
     */
    public function getTitleFr(): string
    {
        return $this->titleFr;
    }

    /**
     * @param string $titleFr
     */
    public function setTitleFr(string $titleFr): void
    {
        $this->titleFr = $titleFr;
    }

    /**
     * @return string
     */
    public function getSummaryEn(): string
    {
        return $this->summaryEn;
    }

    /**
     * @param string $summaryEn
     */
    public function setSummaryEn(string $summaryEn): void
    {
        $this->summaryEn = $summaryEn;
    }

    /**
     * @return string
     */
    public function getSummaryFr(): string
    {
        return $this->summaryFr;
    }

    /**
     * @param string $summaryFr
     */
    public function setSummaryFr(string $summaryFr): void
    {
        $this->summaryFr = $summaryFr;
    }

    /**
     * @return DateTimeInterface
     */
    public function getReleaseDate(): DateTimeInterface
    {
        return $this->releaseDate;
    }

    /**
     * @param DateTimeInterface $releaseDate
     */
    public function setReleaseDate(DateTimeInterface $releaseDate): void
    {
        $this->releaseDate = $releaseDate;
    }

    /**
     * @return float
     */
    public function getAbyYearStart(): float
    {
        return $this->abyYearStart;
    }

    /**
     * @param float $abyYearStart
     */
    public function setAbyYearStart(float $abyYearStart): void
    {
        $this->abyYearStart = $abyYearStart;
    }

    /**
     * @return float|null
     */
    public function getAbyYearEnd(): ?float
    {
        return $this->abyYearEnd;
    }

    /**
     * @param float|null $abyYearEnd
     */
    public function setAbyYearEnd(?float $abyYearEnd): void
    {
        $this->abyYearEnd = $abyYearEnd;
    }

    /**
     * @param ArtworkType $type
     */
    public function setType(ArtworkType $type): void
    {
        $this->type = $type;
    }

    /**
     * @return ArtworkType
     */
    public function getType(): ArtworkType
    {
        return $this->type;
    }
}
