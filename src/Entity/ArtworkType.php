<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ArtworkTypeRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ArtworkTypeRepository::class)
 * @ApiResource()
 */
class ArtworkType
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="string", length=255)
     */
    private string $name;

    /**
     * @ORM\OneToMany(targetEntity="Artwork", mappedBy="type")
     *
     * @var Collection<int, Artwork>
     */
    private Collection $artworks;

    /**
     * ArtworkType constructor.
     *
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->setName($name);
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->getName();
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, Artwork>
     */
    public function getArtworks(): Collection
    {
        return $this->artworks;
    }

    /**
     * @param Collection<int, Artwork> $artworks
     *
     * @return $this
     */
    public function setArtworks(Collection $artworks): self
    {
        $this->artworks = $artworks;

        return $this;
    }

    /**
     * @param Artwork $artwork
     *
     * @return $this
     */
    public function addArtwork(Artwork $artwork): self
    {
        $this->artworks->add($artwork);

        return $this;
    }
}
