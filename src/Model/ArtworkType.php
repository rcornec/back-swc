<?php

namespace App\Model;

class ArtworkType
{
    public const ANIMATED_MOVIE = 'ANIMATED_MOVIE';
    public const ANIMATED_SERIE = 'ANIMATED_SERIE';
    public const BOOK = 'BOOK';
    public const COMIC = 'COMIC';
    public const MOVIE = 'MOVIE';
    public const SERIE = 'SERIE';
    public const VIDEO_GAME = 'VIDEO_GAME';

    /**
     * @param string $type
     *
     * @return bool
     */
    public static function exists(string $type): bool
    {
        return in_array($type, [
            self::ANIMATED_MOVIE,
            self::ANIMATED_SERIE,
            self::BOOK,
            self::COMIC,
            self::MOVIE,
            self::SERIE,
            self::VIDEO_GAME,
        ]);
    }
}
