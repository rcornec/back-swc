# STAR WARS CANON - BACKEND
[![pipeline status](https://gitlab.com/rcornec/back-swc/badges/master/pipeline.svg)](https://gitlab.com/rcornec/back-swc/commits/master)
[![coverage report](https://gitlab.com/rcornec/back-swc/badges/master/coverage.svg)](https://gitlab.com/rcornec/back-swc/commits/master)

## Purpose
The objectif of this project, is to reference all Star Wars canon artworks (Movies, Series, Book, Comics, Games, ...)
And show them in a timeline ordered by the BBY they start.  

## Tech Choice
### Backend
 - PHP 7.4
 - Symfony 5
 - API Platform (REST)

### Frontend
 - ReactJS

### DB
 - PostgreSQL for reference all necessary data
 - Redis to show them in the timeline fastly
 
### Others
 - Integration : Gitlab and Gitlab CI
 - Deployment : Clever cloud
 
## Installation
Juste type this command, and it will be works
 - `make build` : Create .env, pull, build and up containers + symfony server
 - `make migration` : Migrate data in `src/Migrations` to create database
 
 Now, if you go to `localhost:8000` you will see the starting page.
 And voilà!
 
## Make help
 You can type the `make` command to show the list of availables commands:
 
```bash
build                          install the project
csfixer                        Run php-cs-fixer
dbmigrate                      doctrine migrations all
install                        install vendors
start                          start containers and server
stop                           stop containers and server
tests                          Unit test
update                         update vendors
```