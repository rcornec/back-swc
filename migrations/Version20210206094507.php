<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210206094507 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Tables schema';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            'postgresql' !== $this->connection->getDatabasePlatform()->getName(),
            'Migration can only be executed safely on \'postgresql\'.'
        );

        $this->addSql('CREATE TABLE artwork_type (name VARCHAR(255) NOT NULL, PRIMARY KEY(name))');
        $this->addSql(<<<'SQL'
            INSERT INTO artwork_type (name) VALUES
                ('ANIMATED_MOVIE'),
                ('ANIMATED_SERIE'),
                ('BOOK'),
                ('COMIC'),
                ('MOVIE'),
                ('SERIE'),
                ('VIDEO_GAME')
        SQL);

        $this->addSql(<<<'SQL'
            CREATE TABLE artwork (
                id INT NOT NULL,
                type VARCHAR(255) DEFAULT NULL,
                title_en VARCHAR(255) NOT NULL,
                title_fr VARCHAR(255) NOT NULL,
                summary_en TEXT NOT NULL,
                summary_fr TEXT NOT NULL,
                release_date TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
                aby_year_start FLOAT NOT NULL,
                aby_year_end FLOAT,
                PRIMARY KEY(id)
            )
        SQL);
        $this->addSql('CREATE INDEX idx_881fc5768cde5729 ON artwork (type)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            'postgresql' !== $this->connection->getDatabasePlatform()->getName(),
            'Migration can only be executed safely on \'postgresql\'.'
        );

        $this->addSql('DROP TABLE artwork');
        $this->addSql('DROP TABLE artwork_type');
    }
}
