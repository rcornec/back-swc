.PHONY: help

help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

build: ## install the project
	cp .env.dev .env
	docker-compose pull
	docker-compose up --build -d
	symfony server:start -d
	docker-compose ps
	composer install

coverage: ## PHPUnit test coverage
	vendor/bin/simple-phpunit --coverage-text --coverage-xml=tests/_report/coverage-xml --log-junit=tests/_report/coverage-xml/junit.xml

csfixer: ## Run php-cs-fixer
	vendor/bin/php-cs-fixer --config=.php_cs.dist fix

dbmigrate: ## doctrine migrations all
	bin/console doctrine:migrations:migrate

infection: ## Infection test
	vendor/bin/infection

install: ## install vendors
	composer install

phpstan: ## phpstan
	vendor/bin/phpstan analyse -l 8 -c phpstan.neon --no-progress src

start: ## start containers and server
	docker-compose up -d
	symfony server:start -d

stop: ## stop containers and server
	symfony server:stop
	docker-compose stop

tu: ## Unit test
	vendor/bin/simple-phpunit

update: ## update vendors
	composer update
